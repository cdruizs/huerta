let miMapa = L.map('mapid'); 

miMapa.setView([4.6325305,-74.18437], 13);
let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png')
miProveedor.addTo(miMapa);
 


let miGeoJson=L.geoJson(sitio);

miGeoJson.addTo(miMapa);


let miMarcador = L.marker([4.6325305,-74.18437]);
miMarcador.addTo(miMapa);

miMarcador.bindPopup("<b>Arveja</b><br>Plantada:2021/01/12<br>Agricultor:Carlos Daniel Ruiz<br>Nombre cientifico:Pisym sativum").openPopup();
miMarcador.addTo(miMapa);

let miMarcador2 = L.marker([4.6325305,-74.18437]);
miMarcador.addTo(miMapa);

//JSON
let circle = L.circle([4.6325305,-74.18437], {
    color: 'green',
    fillColor: 'green',
    fillOpacity: 0.5,
    radius: 10

});
circle.addTo(miMapa);

